import { Request, Response } from 'express';
import got from 'got';

interface Bank {
    name: string;
    id:   string;
}

export const getBanks = async (req: Request, res: Response) => {
    try {
        const resp = await got.get(process.env.BANKS_URL!).json<{banks: Bank[]}>();
        
        if (resp) {
            res.status(200).send({ok: true, banks: resp.banks});
        }else{
            res.status(400).send({ok: false});
        }
    } catch (error) {
        res.status(500).json({ok: false});
    }
}

