import { Model, Schema } from 'mongoose';
import mongoose from 'mongoose';
import { bankName } from './account';

export interface AddresseeInterface {
    name: string;
    rut: string;
    email: string;
    balance: number;
    accountType: string;
    bank: string;
    accountNumber: number;
    phoneNumber: string,
}

const AddresseeSchema = new Schema<AddresseeInterface>({
    name: { type: String, required: true },
    rut: { type: String, required: true, unique: true },
    email: { type: String, required: true },
    balance: { type: Number, default: 0 },
    accountType: {type: String, required: true},
    bank: {type: String, default: bankName},
    accountNumber: {type: Number},
    phoneNumber: {type: String},
}, { timestamps: true });

AddresseeSchema.method('toJSON', function () {
    const { __v, password, _id, createdAt, updatedAt, balance, ...object } = this.toObject();
    object.uid = _id;
    return object;
});

export const Addressee: Model<AddresseeInterface> = mongoose.model('Addressee', AddresseeSchema);