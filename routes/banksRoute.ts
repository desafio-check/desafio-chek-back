import { Router } from 'express';

import { getBanks } from '../controllers/banksController';
import { validarJWT } from '../middlewares/validate-jwt';

const router = Router();

router.get('/getBanks', validarJWT, getBanks);

export default router;