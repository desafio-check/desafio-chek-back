import { Router } from 'express';

import { login, register, renewToken } from '../controllers/authController';
import { validarJWT } from '../middlewares/validate-jwt';

const router = Router();

router.post('/login', login);
router.post('/register', register);
router.get('/renew', validarJWT, renewToken);

export default router;