import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export interface Token {
    uid: string;
    decoded: number;
    exp: number;
}

export const validarJWT = async (req: Request, res: Response, next: NextFunction) => {

    const token = req.header('x-token');

    if (!token) {
        return res.status(401).json({
            ok: false,
            msg: 'No hay token en la petición'
        }); 
    }

    try {
        const { uid } = (jwt.verify(token, process.env.SECRETKEY_JWT!) as Token);

        req.body.uid = uid;

        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({
            ok: false,
            msg: 'Token no válido'
        })
    }

}
