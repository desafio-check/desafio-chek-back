import { Model, Schema } from 'mongoose';
import mongoose from 'mongoose';

export const bankName = "MY_BANK";
export interface AccountInterface {
    name: string;
    rut: string;
    email: string;
    password: string;
    balance: number;
    accountType: string;
    bank: string;
    accountNumber: number;
    phoneNumber: string;
    addressees: string[];
}

const AccountSchema = new Schema<AccountInterface>({
    name: { type: String, required: true },
    rut: { type: String, required: true, unique: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    balance: { type: Number, default: 0 },
    accountType: {type: String, default: "CUENTA VISTA"},
    bank: {type: String, default: bankName},
    accountNumber: {type: Number},
    phoneNumber: {type: String},
    addressees: [{type: mongoose.Types.ObjectId, ref: 'Addressee'}]
}, { timestamps: true });

AccountSchema.method('toJSON', function () {
    const { __v, password, _id, balance, ...object } = this.toObject();
    object.uid = _id;
    return object;
});

export const Account: Model<AccountInterface> = mongoose.model('Account', AccountSchema);