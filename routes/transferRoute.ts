import { Router } from 'express';
import { makeTransfer, getTransfers } from '../controllers/transferController';
import { validarJWT } from '../middlewares/validate-jwt';

const router = Router();

router.post('/makeTransfer', validarJWT, makeTransfer);
router.get('/getTransfers', validarJWT, getTransfers);

export default router;