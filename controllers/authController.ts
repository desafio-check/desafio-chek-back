import { NextFunction, Request, Response } from 'express';
import bcrypt from 'bcrypt';
import { generarJWT } from '../helpers/generate-jwt';
import { Account, AccountInterface } from '../models/account';
import { randomBalance } from '../utils/utils';

export const login =  async (req: Request, res: Response) => {
    try {
        const { rut, password } = req.body;

        // Verificar si la cuenta existe
        const account = await Account.findOne({ rut });

        if (!account) {
            return res.status(400).json({
                ok: false,
                msg: 'Rut o contraseña incorrectos.'
            });
        }
    
        // Verificar la contraseña
        const validPassword = bcrypt.compareSync(password, account.password!);
        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'Rut o contraseña incorrectos.'
            });
        }
        
        // Generar el JWT
        const token = await generarJWT(account.id);
    
        res.json({
            ok: true,
            account: returnAccount(account),
            token
        });
    } catch (error) {
        res.status(500).json({ok: false, msg: 'Error'});
    }
}

export const register =  async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { name, rut, email, password, phoneNumber } = req.body;

        const exists = await Account.findOne({ rut });
        if (exists) {
           return res.status(400).send({ok: false, msg: `La cuenta con el rut: ${rut} ya se encuentra registrada.`});
        } 

        const pass = await bcrypt.hash(password, 10);
        const account = await Account.create({
          name,
          rut,
          email,
          password: pass,
          accountNumber: parseInt(rut.split("-")[0]),
          balance: randomBalance(),
          phoneNumber
        });

        const token = await generarJWT(account.id);

        res.send({ 
            ok: true, 
            account: returnAccount(account),
            token
        });
    } catch (error) {
        res.status(500).send({ok: false, msg:"Error"});
    }
}

export const renewToken = async (req: Request, res: Response) => {

    const { uid } = req.body;

    try {
        const account = await Account.findById(uid);

        if (!account) {
            return res.status(400).json({
                ok: false,
                msg: 'Usuario no encontrado.'
            });
        }

        const token = await generarJWT(account.id);

        res.json({
            ok: true,
            account: returnAccount(account),
            token
        });
        
    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Error'
        });
    }
}


const returnAccount = (account:AccountInterface) => {
    return {
        name: account.name,
        rut: account.rut,
        email: account.email,
        balance: account.balance,
        accountType: account.accountType,
        bank: account.bank,
        accountNumber: account.accountNumber
    }
} 