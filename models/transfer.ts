import { Model, ObjectId, Schema } from 'mongoose';
import mongoose from 'mongoose';

export interface TransferInterface {
    mount: number;
    bank: string;
    account: ObjectId;
    addressee: ObjectId;
}

const TransferSchema = new Schema<TransferInterface>({
    mount: {type: Number, required: true },
    bank: {type: String, required: true},
    account: {type: mongoose.Types.ObjectId, ref: 'Account', required: true},
    addressee: {type: mongoose.Types.ObjectId, ref: 'Addressee', required: true}
}, { timestamps: true });

TransferSchema.method('toJSON', function () {
    const { __v, password, _id, ...object } = this.toObject();
    object.uid = _id;
    return object;
});

export const Transfer: Model<TransferInterface> = mongoose.model('Transfer', TransferSchema);