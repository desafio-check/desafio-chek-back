import { Router } from 'express';
import { addAddressee, getAddressee } from '../controllers/accountContoller';
import { validarJWT } from '../middlewares/validate-jwt';

const router = Router();

router.post('/addAddressee', validarJWT, addAddressee);
router.get('/getAddressee', validarJWT, getAddressee);

export default router;