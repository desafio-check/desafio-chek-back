import express, { Application } from 'express';
// import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';

import { connectDB } from './db/connection';
import authRoute from './routes/authRoute';
import accountRoute from './routes/accountRoute';
import transferRoute from './routes/transferRoute';
import banksRoute from './routes/banksRoute';

//config
const app: Application = express();
const PORT = process.env.PORT || 3000;
require('dotenv/config');

//middlewares
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

//Rutas
app.use('/auth', authRoute);
app.use('/account', accountRoute);
app.use('/transfer', transferRoute);
app.use('/banks', banksRoute);

//levantar ambiente
connectDB();
app.listen(PORT, () => {
    console.log('Listening on Port: ' + PORT);
});