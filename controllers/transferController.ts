import { Request, Response } from 'express';
import { Addressee } from '../models/addressee';
import { Account, bankName } from '../models/account';
import { Transfer } from  '../models/transfer';

export const makeTransfer = async (req: Request, res: Response) => {

    try {
        const { mount, bank, rutAccount, rutAddressee } = req.body;

        if(mount <= 0 ) return res.status(400).send({ok: false, msg: "Monto debe ser mayor a 0."});
        
        const account = await Account.findOne({rut: rutAccount});
        const addressee = await filterBank(bank, rutAddressee);

        if (addressee && mount <= account!.balance) {
            const accountBalance = account!.balance - mount;
            const addresseeBalance = addressee.balance + mount;

            await Account.findByIdAndUpdate(account!._id, { balance: accountBalance});
            await transferMoneyAddressee(bank, addressee._id.toString(), addresseeBalance);

            await Transfer.create({mount, bank, account:account!._id, addressee: addressee._id});

            res.status(200).send({
                ok: true, 
                msg: `Transferencia realizada exitosamente hacia ${addressee.name} con numero de cuenta ` + 
                     `${addressee.accountNumber} por un monto de $${mount}`
            });
        }else{
            res.status(400).send({ok: false, msg: "Monto insuficiente."});
        }
        
    } catch (error) {
        console.log(error)
        res.status(500).send({ok: false, msg: 'Error'});
    }
}

export const getTransfers = async (req: Request, res: Response) => {
    try {
        const { uid } = req.body;

        const transfers = await Transfer.find({account: uid}, {mount:1, createdAt:1})
            .populate('addressee', ['name', 'rut', 'bank', 'accountType'])
            .sort({createdAt:-1});

        res.status(200).send({ok: true, transfers});
    } catch (error) {
        res.status(500).send({ok: false, msg: "Error"});
    }
}

const filterBank = async (bank:string, rutAddressee:string) => {
    return bankName === bank 
            ? await Account.findOne({rut: rutAddressee}) 
            : await Addressee.findOne({rut: rutAddressee});
}

const transferMoneyAddressee = async (bank:string, uid:string, balance: number) => {
    return bankName === bank 
        ? await Account.findByIdAndUpdate(uid, { balance })
        : await Addressee.findByIdAndUpdate(uid, { balance });
} 