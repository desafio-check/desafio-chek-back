import mongoose from 'mongoose';

export const connectDB= async () => {
    mongoose.connect(process.env.DB_CONNECTION!, () => {
        console.log('Connected to db');
    });
}

