import { Request, Response } from 'express';
import { Addressee } from '../models/addressee';
import { Account } from '../models/account';

import { randomBalance } from '../utils/utils';

export const addAddressee = async (req: Request, res: Response) => {
    try {
        const { uid, addressee } = req.body;

        const exists = await Addressee.findOne({rut: addressee.rut});
        if (exists) {
            const account = await Account.findById(uid);
            if (account?.addressees.includes(exists.id)) {
                return res.status(200).send({ok: true, msg: 'El destinario ya se encuentra en tu lista.'});
            }
    
            account?.addressees.push(exists.id);
            await account?.save();
    
            res.status(200).send({ok: true,  msg: 'Destinatario agregado correctamente.'});
          }else{
            const newAddressee = await Addressee.create({
                ...addressee, 
                balance: randomBalance(),
                accountNumber: parseInt(addressee.rut.split("-")[0])
            });

            const account = await Account.findById(uid);
            account?.addressees.push(newAddressee.id);
            await account?.save();
    
            res.status(200).send({ok: true, msg: 'Destinatario agregado correctamente.'});
          }
    } catch (error) {
        console.log(error)
        res.status(500).send({ok: false, msg: 'Error'});
    }
}

export const getAddressee = async (req: Request, res: Response) => {
    try {
        const { rutAddressee } = req.query;
        const { uid } = req.body;

        const account = await Account.findOne({user: uid});
        const addressee = await Addressee.find({_id:{$in: account?.addressees}, rut: rutAddressee});

        addressee.length > 0 
            ? res.status(200).send({ok: true, addressee: addressee[0]}) 
            : res.status(404).send({ok: false, msg: `Destinatario con rut ${rutAddressee} no encontrado.`});
        
    } catch (error) {
        res.status(500).send({ok: false, msg: 'Error'});
    }
}